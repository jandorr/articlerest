//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
// const _ = require('lodash');
const mongoose = require('mongoose');

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}



mongoose.connect('mongodb://localhost/wikiDB', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

    console.log("Connected to database.");

    const articleSchema = new mongoose.Schema({
        title: String,
        content: String,
    });
    const Article = mongoose.model('Article', articleSchema);
    article = new Article({title: "Kimi", content: "hello"});
    // article.save();

    const app = express();
    app.set('view engine', 'ejs');
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(express.static("public"));

    app.get('/', function (req, res) {
        res.render('home.ejs', {})
    });

    // Requests targeting all articles. ////////////////////////////////////////
    // Chained get, post and delete:
    app.route('/articles')

    .get(function (req, res) {
        Article.find(function (err, articles) {
            if (!err) {
                res.send(articles);
            } else {
                res.send(err);
            }
        });
    })

    .post(function (req, res) {
        const newArticle = new Article({
            title: req.body.title,
            content: req.body.content,
        });

        newArticle.save(function (err, savedArticle) {
            if (!err) {
                const msg = 'Saved: ' + savedArticle;
                console.log(msg);
                res.send(msg);
            } else {
                console.err(err);
                res.send(err);
            }
        });
    })

    .delete(function (req, res) {
        Article.deleteMany(function (err) {
            if (!err) {
                msg = "Successfully deleted all articles.";
                console.log(msg);
                res.send(msg);
            } else {
                console.log(err);
                res.send(err);
            }
        });
    });

    // Requests targeting a specific article. //////////////////////////////////
    // Chained:
    app.route('/articles/:articleTitle')

    .get(function (req, res) {
        Article.findOne({title: req.params.articleTitle}, function (err, foundArticle) {
            if (foundArticle) {
                res.send(foundArticle);
            } else {
                res.send("No article matching title: " + req.params.articleTitle);
            }
        });
    })

    .put(function (req, res) {
        // {overwrite: true} will replace the entire document.
        // {overwrite: false} will make missing fields null.

        Article.update(
            {title: req.params.articleTitle},
            {title: req.body.title, content: req.body.content},
            {overwrite: true},
            function (err) {
                if (!err) {
                    res.send('Successfully updated article.');
                }
                else {
                    console.error(err);
                    res.send(err);
                }
            }
        );
    })

    .patch(function (req, res) {
        // $set operator replaces the value of an existing field with the specified value.
        // req.body e.g.: {content: "foo"}

        Article.update(
            {title: req.params.articleTitle},
            {$set: req.body},
            function (err) {
                if (!err) {
                    res.send('Successfully updated article.');
                } else {
                    res.send(err);
                }
            }
        );
    })

    .delete(function (req, res) {
        Article.deleteOne(
            {title: req.params.articleTitle},
            function (err) {
                if (!err) {
                    res.send('Deleted ' + req.params.articleTitle);
                } else {
                    res.send(err);
                }
            }
        );
    });






    // Start server ////////////////////////////////////////////////////////////
    app.listen(port, function() {
        console.log("Server started on port " + port);
    });

});
